﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameLoader : MonoBehaviour 
{

	public static GameLoader Instance = null;

	public GameObject[] initialLoadingScreen;

	 

	void OnEnable()
	{
		
	}
	void Awake()
	{
		Instance = this;
		
		
	}
	// Use this for initialization
	void Start () 
	{
		 
		StartGame();
	}

	void ShowInitialLoadingScreen()
	{
		
		int r = UnityEngine.Random.Range(0, initialLoadingScreen.Length);
		for(int i=0;i<initialLoadingScreen.Length;i++)
		{
			if(i==r)
				initialLoadingScreen[r].SetActive(true);
			else
				initialLoadingScreen[r].SetActive(false);	
		}
		

	}
 
	
	public void StartGame()
	{
		 if(GameManager.Instance.clearLocalData )
		   PlayerPrefs.DeleteAll(); //--> to clear player pref locally...
		    
		 
	}


	public void TapToContinueButton()
	{
		LoadMainScene();
	}
	 
	public void LoadMainScene()
	{  
		 
		
	}

	 
	
	 
}
