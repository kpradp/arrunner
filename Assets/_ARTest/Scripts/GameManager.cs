﻿using UnityEngine;
using Prime31;
using System.Collections;
 
 

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GameManager>();
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    private string loadedLevel = "";
    public bool clearLocalData;
    public bool isTestingMode;

    public string LoadedLevel
    {
        get
        {
            return loadedLevel;
        }

        set
        {
            loadedLevel = value;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != instance)
                Destroy(gameObject);
        }
    }

    void OnEnable()
    {
 
    }

    void OnDisable()
    {

    }

    public void SavePlayerDataLocal()
    {
        
    }
 
    public void GamePause()
    {
        //if(!gamePause)
        {
            GlobalVariables.gamePaused = true;
            Debug.Log("Game is paused");
            GameEventManager.OnGamePaused();
            Time.timeScale = 0;
        }
    }

    public void GameResume()
    {
        //if(gamePause)
        {
            GlobalVariables.gamePaused = false;
            Debug.Log("Game resumed");
            GameEventManager.OnGameResumed();
            Time.timeScale = 1.0f;
        }
    }

    public void GameRestart()
    {
        GlobalVariables.gamePaused = false;
        Debug.Log("Game restarted");
        Time.timeScale = 1.0f;
        GameEventManager.OnGameRestart();

    }

    public void UpdateUI()
    {
        GameEventManager.OnUpdateUI();
    }

    public void SceneStart()
    {
        GameEventManager.OnSceneStart();
    }

    public void SceneEnd()
    {
        Time.timeScale = 1.0f;
        GameEventManager.OnSceneEnd();
    }

    
    void OnApplicationFocus(bool focusStatus)
    {
        
    }

    
    void OnApplicationPause(bool pauseStatus)
    {

    }
 
    void OnApplicationQuit()
    {
         
        // clear notification
    }
}
