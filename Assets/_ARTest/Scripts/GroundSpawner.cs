﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31;
using System;

public class GroundSpawner : MonoBehaviour
{

    GameObject floorStart;
    public GameObject spawnerParent;
    public GameObject startTile;
    public List<GameObject> floorPrefabs;
    public GameObject playerRef;
    
    public int numberOfObjectSpawn = 4;
    private Queue<GameObject> spawnFloorQueue;
    public static event Action  SpawnEvent ,RemoveSpawnEvent;

    float distanceTravelled = 0;
    Vector3 lastPosition;


    private void OnEnable()
    {
        SpawnEvent += GroundSpawner_SpawnEvent;
        RemoveSpawnEvent += GroundSpawner_RemoveSpawnEvent;
    }


    void Start()
    {
        floorStart = startTile;
        playerRef = GameObject.FindGameObjectWithTag("Player");
        lastPosition = playerRef.transform.position;
        spawnFloorQueue = new Queue<GameObject>(numberOfObjectSpawn);

    }


    private void GroundSpawner_SpawnEvent()
    {
        Debug.Log( "Event Invoked");
        if (floorStart.activeSelf) floorStart.SetActive(false);
        Spawn();
        // SpawnFloor();
        distanceTravelled = 0;
    }

    private void GroundSpawner_RemoveSpawnEvent()
    {
      
         ReSpawnFromQueue();
        Debug.Log("removing old spawns ");
    }

    // Start is called before the first frame update
   
    void ReSpawnFromQueue ()
    {
        var c = spawnFloorQueue.Dequeue();
        c.transform.position = startTile.transform.GetChild(0).transform.position;
        c.transform.parent = spawnerParent.transform;
        spawnFloorQueue.Enqueue(c);

    }

    void Spawn()
    {
     
        for (int i = 0; i < numberOfObjectSpawn; i++)
        {
            var rand = UnityEngine.Random.Range(0, 2);
            var c = GameObject.Instantiate(floorPrefabs[rand]) as GameObject;
            c.transform.position = startTile.transform.GetChild(0).transform.position;
            c.transform.parent = spawnerParent.transform;
            startTile = c;
            
            spawnFloorQueue.Enqueue(c);

        }
         
        UIManager.Instance.canStart = false;
    }

    

    // Update is called once per frame
    void Update()
    {
        if (UIManager.Instance.canStart)
            Spawn();//SpawnFloor();

        distanceTravelled += Vector3.Distance(playerRef.transform.position, lastPosition);
        lastPosition = playerRef.transform.position;
       // Debug.Log("distance travelled " + distanceTravelled);

        if (distanceTravelled >= 1.5)
        {
            SpawnEvent?.Invoke();
            RemoveSpawnEvent.Invoke();
        }
    }
}
