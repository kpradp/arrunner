﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using ARTest;
public class UIManager : MonoBehaviour
{
    public Button startButton;
    public Button resetButton;
    public Button pauseButton;
    public Button placeLevel;
    public Button stopPlaneTracking;
    public Button trackPlane;
    
    public ARSession arSession;
    public GameObject playerRef;
    public GameObject
        spawnner;
    public bool canStart = false;
    public static UIManager Instance = null;

    public bool levelPlaced = false;

    //public GameObject placeMarkerPanel, placeLevelPanel, startPanel;
    //public Button placePlaneOK;
    //public Button placeLevelOK;
    //public Button startOK;

    public Text instructionText;
    public ARPlaneManager aRPlaneManager;

    private void Awake()
    {

        Instance = this;
        aRPlaneManager = GetComponent<ARPlaneManager>();
        resetButton.onClick.AddListener(Reset);
        startButton.onClick.AddListener(OnStart);
        pauseButton.onClick.AddListener(OnPausedClicked);
        placeLevel.onClick.AddListener(OnPlaceLevel);
        stopPlaneTracking.onClick.AddListener(StopPlaneTracking);
        trackPlane.onClick.AddListener(TrackPlane);
       
        GameEventManager.GameStart += GameEventManager_GameStart;
        GlobalVariables.numOfLives = 3;


        //placePlaneOK.onClick.AddListener(PlaceMarkerOK);
        //placeLevelOK.onClick.AddListener(OnPlaceLevel);
        //startOK.onClick.AddListener(StartPanelOK);
        instructionText.text = "Touch the plane to place the marker";
       // GameObject.Instantiate(spawnner);

    }

    void Start()
    {
        //placeMarkerPanel.SetActive(false);
        //placeLevelPanel.SetActive(false);
        //startPanel.SetActive(false);
    }

    //void PlaceMarkerOK()
    //{
    //    placeMarkerPanel.SetActive(false);
        
    //    //placeLevelPanel.SetActive(true);

    //}
    //void StartPanelOK()
    //{
    //    startPanel.SetActive(false);
    //}

    private void TrackPlane()
    {
        PlaceOnPlane.Instance.aRPlaneManager.enabled = true;
         
    }

    private void GameEventManager_GameStart()
    {
        GlobalVariables.numOfLives = 3;
    }
    private void StopPlaneTracking()
    {
        PlaceOnPlane.Instance.aRPlaneManager.enabled = false;
    }

    void OnPlaceLevel()
    {
        levelPlaced = true;

        instructionText.text = "Click Place on Plane";
        //placeLevelPanel.SetActive(false);
        //startPanel.SetActive(true);
    }

    private void OnPausedClicked()
    {
        // Player.Instance.startMovement = false;
      
        
    }

    private void OnStart()
    {
      
        //GlobalVariables.isObjectPlaced = false;
        
        canStart = true;
        UIManager.Instance.instructionText.text = "";
    }

    
    public void Reset()
    {

        GlobalVariables.isObjectPlaced = false;
        arSession.Reset();
    }


  
    // Update is called once per frame
    void Update()
    {
        
    }
}
