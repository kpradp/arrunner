﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVariables : MonoBehaviour
{
    public static bool gamePaused;
    public static bool gameResumed;
    public static bool isPlaneDiscovered;
    public static bool isObjectPlaced = false;

    public static int numOfLives;

}
