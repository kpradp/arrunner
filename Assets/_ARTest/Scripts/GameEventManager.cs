﻿using UnityEngine;
using System;

public class GameEventManager : MonoBehaviour
{
    public static event Action PlayerDead, SceneStart,GameStart, GamePaused, GameResumed, GameRestart,GameEnd, UpdateUI, SceneEnd ,GameOver;

    public static event Action SpawnEvent, RemoveSpawnEvent;
    public static void OnSceneStart() => SceneStart?.Invoke();

    public static void OnPlayerDead() => PlayerDead?.Invoke();
    public static void OnGameStart() => GameStart?.Invoke();
    public static void OnGamePaused() => GamePaused?.Invoke();


    public static void OnGameResumed() => GameResumed?.Invoke();

    public static void OnGameRestart() => GameRestart?.Invoke();
    public static void OnGameEnd() => GameEnd?.Invoke();
    public static void OnUpdateUI() => UpdateUI?.Invoke();

    public static void OnSpawn() => SpawnEvent?.Invoke();
    public static void OnRemoveSpawn() => RemoveSpawnEvent?.Invoke();

    public static void OnGameOver() => GameOver?.Invoke();
    public static void OnSceneEnd() => SceneEnd?.Invoke();

}
