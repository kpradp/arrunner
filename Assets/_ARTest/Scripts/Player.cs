﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31;

namespace ARTest
{


    public class Player : MonoBehaviour
    {

        public static Player Instance = null;
        public bool startMovement = false;
        public float movementSpeed;

        TKSwipeRecognizer recognizer;

        Vector3 lastPosition;
        float distanceTravelled = 0;

        private void Awake()
        {
            Instance = this;
        }

        private void OnEnable()
        {
            
            GameEventManager.PlayerDead += GameEventManager_PlayerDead;
        }

        

        private void GameEventManager_PlayerDead()
        {

            Debug.Log("calling player player dead");
        }

        

        private void GameEventManager_SceneEnd()
        {

        }

        void Start()
        {
            recognizer = new TKSwipeRecognizer();
            recognizer.gestureRecognizedEvent += Recognizer_GestureRecognizedEvent;
            TouchKit.addGestureRecognizer(recognizer);
        }


        private void Recognizer_GestureRecognizedEvent(TKSwipeRecognizer r)
        {
            switch (r.completedSwipeDirection)
            {
                case TKSwipeDirection.Left:
                    this.transform.Rotate(this.transform.rotation.x, -90, this.transform.rotation.z);
                    break;

                case TKSwipeDirection.Right:
                    this.transform.Rotate(this.transform.rotation.x, 90, this.transform.rotation.z);
                    break;
            }
            Debug.Log(r.completedSwipeDirection);
        }

        void MovePlayer()
        {
            this.transform.position += transform.forward * Time.deltaTime * (movementSpeed / 100);
        }
        // Update is called once per frame
        void Update()
        {
            if (UIManager.Instance.canStart)
                MovePlayer();

            distanceTravelled += Vector3.Distance(this.transform.position, lastPosition);
            lastPosition = this.transform.position;

            if (this.transform.position.y <= -1)
            {
                GlobalVariables.numOfLives--;
                distanceTravelled = 0;
                GameEventManager.OnPlayerDead();
               
            }


            if (distanceTravelled >= 1.5)
            {
                GameEventManager.OnSpawn();//.SpawnEvent?.Invoke();
                GameEventManager.OnRemoveSpawn();//.RemoveSpawnEvent.Invoke();
                distanceTravelled = 0;
            }
            
        }
    }

}
