﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ARTest;
public class LevelManager : MonoBehaviour
{
    public GameObject spawnerParent;
    public GameObject startTile;
    public List<GameObject> floorPrefabs;
    public GameObject playerRef;

    public GameObject spawnPrefab;

    GameObject spawnFloor;
    GameObject floorStart;
    private Queue<GameObject> spawnFloorQueue;
    public static event Action SpawnEvent, RemoveSpawnEvent;

    Vector3 lastPosition;
    float distanceTravelled = 0;
    public int numberOfObjectSpawn = 4;

    public GameObject referencePoint;
    private void OnEnable()
    {
        GameEventManager.SpawnEvent += GroundSpawner_SpawnEvent;
        GameEventManager.RemoveSpawnEvent += GroundSpawner_RemoveSpawnEvent;
        GameEventManager.PlayerDead += GameEventManager_PlayerDead;
    }

    

    void Start()
    { 
        spawnFloorQueue = new Queue<GameObject>(numberOfObjectSpawn);
        
    }

    public void InstantiateGround()
    {
        referencePoint = GameObject.FindGameObjectWithTag("ReferencePoint");
        spawnFloor = Instantiate(spawnPrefab, referencePoint.transform.position, Quaternion.identity) as GameObject;
        floorStart = spawnFloor.transform.GetChild(0).gameObject;
        startTile = spawnFloor.transform.GetChild(0).gameObject;
        Spawn();
        UIManager.Instance.instructionText.text = "press start to play";
    }
    private void GroundSpawner_SpawnEvent()
    {
        Debug.Log("Event Invoked");
         
        // if (floorStart.activeSelf) floorStart.SetActive(false);
         Spawn();
        UIManager.Instance.levelPlaced = false;


    }
    private void GroundSpawner_RemoveSpawnEvent()
    {

        ReSpawnFromQueue();
        Debug.Log("removing old spawns ");
    }

    void ReSpawnFromQueue()
    {
        var c = spawnFloorQueue.Dequeue();
        c.transform.position = startTile.transform.GetChild(0).transform.position;
        c.transform.parent = spawnerParent.transform;
        spawnFloorQueue.Enqueue(c);
        if (floorStart.activeSelf) floorStart.SetActive(false);
    }

    void Spawn()
    {

        for (int i = 0; i < numberOfObjectSpawn; i++)
        {
            var rand = UnityEngine.Random.Range(0, 2);
            var c = GameObject.Instantiate(floorPrefabs[rand]) as GameObject;
            c.transform.position = startTile.transform.GetChild(0).transform.position;
            c.transform.parent = spawnerParent.transform;
            startTile = c;

            spawnFloorQueue.Enqueue(c);

        }

        UIManager.Instance.levelPlaced = false;
    }

    private void GameEventManager_PlayerDead()
    {
        foreach (var item in spawnFloorQueue)
        {
            Destroy(item);
        }
        spawnFloorQueue.Clear();
        UIManager.Instance.canStart = false;
        Destroy(spawnFloor);
        
    }
    // Update is called once per frame
    void Update()
    {
        if (UIManager.Instance.levelPlaced)
            InstantiateGround();

        
        //Debug.Log(playerRef.transform.position);
        //distanceTravelled += Vector3.Distance(playerRef.transform.position, lastPosition);
        //lastPosition = playerRef.transform.position;
        //Debug.Log("distance travelled " + distanceTravelled);

            //if (distanceTravelled >= 1.5)
            //{
            //    SpawnEvent?.Invoke();
            //    RemoveSpawnEvent.Invoke();
            //}
    }
}
